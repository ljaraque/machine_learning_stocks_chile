clear all; clc, close all;
%slCharacterEncoding('UTF-8')
%% INPUT DATA
    % Update Database?
    updateData=0;       % 0: Do not update      ; 1: Update
    
    % Data Sanity types 
        % Fill back type
        fillBack=1;         % 0: Fill with zeros    ; 1: Fill back with the first value

        % Fill forward type
        fillForward=1;      % 0: Fill with zeros    ; 1: Fill forward with the last value
    
        % Fill gap types
        fillGap=1;          % 0: Fill with zeros    ; 1: Fill with last     ; 2: Fill with next
    % Definition of time period
    initDate=datenum(2011,1,1);
    initDateStr=datestr(initDate,'yyyymmdd');
    [Yi,Mi,Di]=datevec(initDateStr,'yyyymmdd');
    finishDate=datenum(2012,12,31);
    finishDateStr=datestr(finishDate,'yyyymmdd');
    [Yf,Mf,Df]=datevec(finishDateStr,'yyyymmdd');
    
    fprintf(1,'Requested analysis period [ %s to %s ]', initDateStr, finishDateStr); fprintf('\n\n\n');
    % Get data from bolsadesantiago
    ipsaList=importdata('./data/IdxIPSA.csv');
    
    symbols_to_analyze={'all'}; % Use 'all' to update all symbols
    % NOTE: first symbol defines the timestamps. Make sure it includes all trading days of the period
    
%% GENERATION OF SYMBOL LIST
if strcmp(symbols_to_analyze,'all')==1
    symbolList=ipsaList';
else%% else if symbols_to_analyze~='all'
        for i=1:size(symbols_to_analyze,1)
            [i,j]=find(strcmp(ipsaList,symbols_to_analyze(i)));
            if isempty([i j])==1
                fprintf(1,'IPSA Index does not contain symbol: %s', char(symbols_to_analyze(i))); fprintf('\n');
                fprintf(1,'Please check and retry'); fprintf('\n');
                return
            end
        end
        symbolList=symbols_to_analyze;
end


%% Fetch and Update data from Bolsa de Santiago if chosen

    if updateData==1
                fprintf(1,'Extracting Data from Bolsa de Santiago ...'); fprintf('\n\n\n');
                for i=1:size(symbolList,2)
                    symbol=symbolList(i);
                    url=strcat('http://www.bolsadesantiago.com/Theme/Data/Historico.aspx?Symbol=',symbol,'&dividendo=S');
                    filename=strcat('./data/',symbol,'2.csv');
                    urlwrite(char(url),char(filename));
                end
                % Modify files
                fprintf(1,'Converting files to standard format ...'); fprintf('\n\n\n');

                for i=1:size(symbolList,2)
                    symbol=symbolList(i);
                    filename=strcat('./data/',symbol,'2.csv');
                    filename2=strcat('./data/',symbol,'.csv');
                    fid1 = fopen(char(filename), 'r','n');
                    fid = fopen(char(filename2), 'w','n');
                    tline = fgetl(fid1);
                    while ischar(tline)
                        tline=unicode2native(tline);
                        tline(tline==0)=[];
                        %tline=native2unicode(tline);
                        tline=char(tline);
                        tline = strrep(tline, ',', '.');
                        tline = strrep(tline, ';', ',');
                        fprintf(fid, '%s\n', tline);
                        tline = fgetl(fid1);    
                    end
                    fclose(fid1);
                    fclose(fid);
                    delete(char(filename));
                end
    end


%% TIMESTAMPS GENERATION


    symbol='IPSA';
    if updateData==1
        % Download IPSA
                    
                    url=strcat('http://ichart.finance.yahoo.com/table.csv?s=%5EIPSA&d=5&e=24&f=2013&g=d&a=0&b=10&c=2003&ignore=.csv');
                    filename=strcat('./data/',symbol,'2.csv');
                    urlwrite(char(url),char(filename));
        
                    filename=strcat('./data/',symbol,'2.csv');
                    filename2=strcat('./data/',symbol,'.csv');
                    fid1 = fopen(char(filename), 'r','n');
                    fid = fopen(char(filename2), 'w','n');
                    tline = fgetl(fid1);
                    while ischar(tline)
                        tline=unicode2native(tline);
                        tline(tline==0)=[];
                        %tline=native2unicode(tline);
                        tline=char(tline);
                        tline = strrep(tline, '-', '');
                        fprintf(fid, '%s\n', tline);
                        tline = fgetl(fid1);    
                    end
                    fclose(fid1);
                    fclose(fid);
                    delete(char(filename));
                    
                    fprintf(1,'IPSA Loaded and converted'); fprintf('\n\n\n');    
    end         
                    
                    
        % Open file of the first symbol to generate timeVector
        %symbol=symbolList(1);
        filename=strcat('./data/',symbol,'.csv');
        stock=importdata(char(filename));
        stock.data=flipud(stock.data);
        stock.textdata=flipud(stock.textdata); %
        
        
        % Find next present initDate if the specified one is not in the list
        i=1;
        
        while ismember(stock.data(:,1),str2num(initDateStr))==0
            initDateStr=datestr(initDate+i,'yyyymmdd');
            i=i+1;
        end


        
        idxInitDate=ind2sub(size(stock.data(:,1)),find(ismember(stock.data(:,1),str2num(initDateStr))));

        % Find previous finishDate if the specified one is not in the list
        i=1;
        while ismember(stock.data(:,1),str2num(finishDateStr))==0
            finishDateStr=datestr(finishDate-i,'yyyymmdd');
            i=i+1;
        end
        idxFinishDate=ind2sub(size(stock.data(:,1)),find(ismember(stock.data(:,1),str2num(finishDateStr))));


        fprintf(1,'Real analyzed period [ %s to %s ]', initDateStr, finishDateStr); fprintf('\n\n\n');
        
        % Generation of timestamps
        [tvecY,tvecM,tvecD]=datevec(num2str(stock.data(idxInitDate:idxFinishDate,1)),'yyyymmdd');
        vecNumDate=datenum(tvecY,tvecM,tvecD);
        timeVector=datestr(vecNumDate,'dd-mmm-yyyy');
        
        % Fill first layer 1 of 3D database
        % NOTE: This part assumes that first stock is the one to define the valid timestamps
        cube(:,:,1)=stock.data(idxInitDate:idxFinishDate,1:6); % Flipud because Yahoo has dates in reverse order
        
        
%% FILLING 3D DATABASE
        fprintf(1,'Composing 3D-Stocks Database ...'); fprintf('\n\n');

            
        for i=1:size(symbolList,2)
            fprintf(1,'Processing symbol Number: %s',num2str(i));fprintf('\n');
            symbol=symbolList(i);
            filename=strcat('./data/',symbol,'.csv');
            stock=importdata(char(filename));
            % Position of stock in the time window
            [yearSymbol,monthSymbol,daySymbol]=datevec(num2str(stock.data(:,1)),'yyyymmdd');
            vecNumDateSymbol=datenum(yearSymbol,monthSymbol,daySymbol);
            
            for j=1:size(cube,1)
                if isempty(find(vecNumDateSymbol(:,1)==vecNumDate(j)))~=1
                    
                    cube(j,:,i+1)=stock.data(find(vecNumDateSymbol(:,1)==vecNumDate(j)),:);
                else
        
                end
            end
            
              

       % Mode advanced data sanity is pending (fill gaps, detect big daily variations, etc.)
            
                          % Case of using timeseries
        %                 % Generation of timeseries collection and add append timeserie of current symbol
        %                 %ts=timeseries(stock.data(idxInitDate:idxFinishDate,2:6),timeVector);
        %                 %tsc = addts(tsc,ts,symbol);
        %                 tsc = tscollection(timeVector,'name',symbol); % Try with vecNumDate to see ticks in plot
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,2),timeVector),'open');
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,3),timeVector),'high');
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,4),timeVector),'low');
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,5),timeVector),'close');
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,6),timeVector),'volume');
        %                 dataBase.(char(symbol))=tsc;
        
        end
        fprintf(1,'-------------------------- Done!'); fprintf('\n\n');

        
        %% Data Sanity
        fprintf(1,'Running Data Sanity (All Symbols except IPSA ...'); fprintf('\n\n');
        for i=2:size(cube,3)
                    fprintf(1,'Processing symbol Number: %s',num2str(i));fprintf('\n');
                    pause(0.05)
                    firstNonZero=find(cube(:,1,i)~=0, 1, 'first');
                    lastNonZero=find(cube(:,1,i)~=0,1,'last');
                    allZeros=find(cube(:,1,i)==0);
                    if size(allZeros)~=size(cube,1) % Maybe change to: if cube(:,1,i)~=0 
                        gaps=allZeros((find(cube(:,1,i)==0))>firstNonZero & (find(cube(:,1,i)==0))<lastNonZero);
                    else
                        % Fill Whole column with 1
                        cube(:,:,i)=1;
                    end
                    if firstNonZero~=1 & size(allZeros)~=size(cube,1)
                    % Fill back
                        if fillBack==1
                            cube(1:(firstNonZero-1),:,i)=repmat(cube(firstNonZero,:,i),(firstNonZero-1),1);
                            
                        elseif fillBack==0
                            cube(1:(firstNonZero-1),:,i)=0; % Although they were already zero
                        end
                    end
                    if lastNonZero~=size(cube,1) & size(allZeros)~=size(cube,1)
                        % Fill forward
                        if fillForward==1
                            cube((lastNonZero+1):end,:,i)=repmat(cube(lastNonZero,:,i),(size(cube,1)-lastNonZero),1);
                        elseif fillForward==0
                            cube((lastNonZero+1):end,:,i)=0;
                        end
                    end
                    
                    % Fill Gaps
                    if isempty(gaps)~=1
                        if fillGap==1
                            for k=length(gaps):-1:1
                                cube(gaps(k),:,i)=cube(gaps(k)+1,:,i);
                            end
                        elseif fillGap==2
                            for k=1:length(gaps)
                                cube(gaps(k),:,i)=cube(gaps(k)-1,:,i);
                            end
                        elseif fillGap==0
                            cube(gaps)=0;
                        end
                    end
                    
        end      
        fprintf(1,'-------------------------- Done!'); fprintf('\n\n');
        
%     % Normalization
% 
%             for i=1:size(cube,3)
%                 cubeNorm(:,:,i)=featureNormalize(cube(:,:,i));
%             end
% 
%     % VarCovar
%     % Statistical study of data
% 
%     data=permute(cube(:,5,:),[1 3 2]);
%     [varMat,corrMat]=vcMat(data); % Just for reference and debugging
%     figure(1)
%     imagesc(mat2gray(corrMat))
%      P=get(gca,'position');
%      Xscale=get(gca,'Xlim');
%      Yscale=get(gca,'Ylim');
%      hx1=axes('Position',P,'Xlim',Xscale,'Ylim',Yscale,'color','none');
%      set(gca,'yticklabel',[]);
%      colormap(gray)
%     title(sprintf('Correlation Matrix (IPSA Stocks Group), between %s and %s', initDateStr, finishDateStr),'FontSize',14,'FontWeight','bold')
% 
%     xlabel('Symbols (1st: IPSA Index, 2-41: IPSA Components','FontSize',12)
%     y=ylabel('Symbols (1st: IPSA Index, 2-41: IPSA Components','FontSize',12);
%     set(y, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0]);
%     %timeVector
% 
        figure('Color',[1 1 1]);
                ts1=timeseries(cube(:,5,1)/cube(1,5,1),timeVector);
                plot(ts1,'r','Linewidth',1.5)
                hold on;    
                h=hsv(40);
            for i=2:size(cube,3)%29:29%size(cube,3)
                ts1=timeseries(cube(:,5,i)/cube(1,5,i),timeVector);
                plot(ts1,'color',h(i-1,:),'Linewidth',0.1)
                hold on;
            end
                grid on;
                grid minor
    set(gca,'Color',[0.93 0.93 0.93]);
    %title('Normalized Historical Prices (IPSA Stocks Group)','FontSize',16,'FontWeight','bold')
    title(sprintf('Normalized Historical Prices (IPSA Stocks Group), between %s and %s', initDateStr, finishDateStr),'FontSize',14,'FontWeight','bold')
    xlabel('Date','FontSize',12)
    ylabel('Normalized Symbol Price','FontSize',12)
    leg=legend(['IPSA',symbolList],'location','eastoutside');
    set(leg,'FontSize',9)





% %timeVector 
% 
%     figure('Color',[1 1 1]);
%             ts1=timeseries(dailyRet(:,1),timeVector);
%             plot(ts1,'r','Linewidth',1.5)
%             hold on;    
%             h=hsv(40);
%         for i=2:size(dailyRet,2)%29:29%size(cube,3)
%             ts1=timeseries(dailyRet(:,i),timeVector);
%             plot(ts1,'color',h(i-1,:),'Linewidth',0.1)
%             hold on;
%         end
%             grid on;
%             grid minor
% set(gca,'Color',[0.93 0.93 0.93]);
% %title('Normalized Historical Prices (IPSA Stocks Group)','FontSize',16,'FontWeight','bold')
% title(sprintf('Daily return (IPSA Stocks Group), between %s and %s', initDateStr, finishDateStr),'FontSize',14,'FontWeight','bold')
% xlabel('Date','FontSize',12)
% ylabel('Symbol Return','FontSize',12)
% leg=legend(['IPSA',symbolList],'location','eastoutside');
% set(leg,'FontSize',9)


%        % LIN REG using ML course code
% 
%         addpath('linReg/');
% 
%         linReg([cube(:,5,1)/cube(1,5,1) cube(:,5,15)/cube(1,5,15)]);
% 
%         rmpath('linReg/');
% 

% Generation of daily return 3D Database


        closeMat=permute(cube(:,5,:),[1 3 2]);
        dailyRet=zeros(size(closeMat));
        dailyRet(1,:)=0;
        for i=2:size(closeMat,1)
            dailyRet(i,:)=closeMat(i,:)./closeMat(i-1,:)-1;
        end


%       Polyfit Matlab linear regression

        figure('units','normalized','outerposition',[0 0 1 1])
        p=zeros(size(symbolList,2)+1,2);
        
        subplot(6,7,1)
        p= polyfit(dailyRet(:,1),dailyRet(:,1) ,1);   % p returns 2 coefficients fitting r = a_1 * x + a_2
      
        r = p(1,1) .* dailyRet(:,1) + p(1,2); % compute a new vector r that has matching datapoints in x
        plot(dailyRet(:,1), dailyRet(:,1), 'bo','MarkerSize', 3);
        hold on;
        plot(dailyRet(:,1), r, 'k-');
        hold off;
        axis([-0.05 0.05  -0.05 0.05])
        set(gca,'Color',[0.93 0.93 0.93]);
        title(sprintf('IPSA/IPSA'))
        grid on
        
        
        for i=1:(size(dailyRet,2)-1)
        
        subplot(6,7,i+1)
        p(i+1,:) = polyfit(dailyRet(:,1),dailyRet(:,i+1) ,1);   % p returns 2 coefficients fitting r = a_1 * x + a_2
        r = p(i+1,1) .* dailyRet(:,1) + p(i+1,2); % compute a new vector r that has matching datapoints in x
        plot(dailyRet(:,1), dailyRet(:,i+1), 'bo','MarkerSize', 3);
        hold on;
        plot(dailyRet(:,1), r, 'k-');
        hold off;
        axis([-0.05 0.05  -0.05 0.05])
        set(gca,'Color',[0.93 0.93 0.93]);
        title(sprintf('IPSA/%s (ID:%d)',symbolList{i},i))
        grid on
        
        end
        
ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');

text(0.5, 1,'\bf DAILY RETURNS REGRESSION IPSA AND COMPONENTS','HorizontalAlignment','center','VerticalAlignment', 'top')

% Plot of 1 regression to choose
        figure(6)
        symbolReg=6;
        d= polyfit(dailyRet(:,1),dailyRet(:,symbolReg) ,1);   % p returns 2 coefficients fitting r = a_1 * x + a_2
        r = d(1) .* dailyRet(:,1) + d(2); % compute a new vector r that has matching datapoints in x
        plot(dailyRet(:,1), dailyRet(:,symbolReg), 'bx','MarkerSize', 7);
        hold on;
        plot(dailyRet(:,1), r, 'k-');
        hold off;
        axis([-0.05 0.05  -0.05 0.05])
        set(gca,'Color',[0.93 0.93 0.93]);
        title(sprintf('IPSA vs %s (ID:%d)',symbolList{symbolReg},symbolReg))
        grid on

% Calculation of betas and alfas through regression
regressions=[[1:41]' p]

% VarCovar of daily returns
% Statistical study of returns

[varMat,corrMat]=vcMat(dailyRet); % Just for reference and debugging
% figure(7)
%TO SAVE FIGURE: %figure('Visible','off','units','normalized','outerposition',[0.2 1 0.5 0.7])
figure('units','normalized','outerposition',[0.2 1 0.5 0.7])
a=imagesc(corrMat),colorbar
 P=get(gca,'position');
 Xscale=get(gca,'Xlim');
 Yscale=get(gca,'Ylim');
 hx1=axes('Position',P,'Xlim',Xscale,'Ylim',Yscale,'color','none');
 set(gca,'yticklabel',[]);
 colormap(gray)
title(sprintf('Correlation Matrix of Daily Returns (IPSA Stocks Group), between %s and %s', initDateStr, finishDateStr),'FontSize',14,'FontWeight','bold')

xlabel('Symbols (1st: IPSA Index, 2-41: IPSA Components','FontSize',12)
y=ylabel('Symbols (1st: IPSA Index, 2-41: IPSA Components','FontSize',12);
set(y, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0]);

% TO SAVE FIGURE: % saveas(a,'corr.jpg','jpg');

% Calculation of betas through covariance matrix
betaMat=varMat/varMat(1,1);


%% Portfolio

% portfolio weigths
w=zeros(size(symbolList,2),1)+1/(size(symbolList,2));
% symbols expected returns
muSymbols=(mean(dailyRet(:,2:end),1))';
% symbols std
sigmaSymbols=(std(dailyRet(:,2:end),0,1))';
% symbols sharp ratio
sSymbols=muSymbols./sigmaSymbols;

% Portfolio expected return
muPort=w'*muSymbols;
sigmaPort=sqrt(w'*varMat(2:end,2:end)*w);

% NOTA: Ver por qu? muSymbols(1) es distinto a muPort si ambos son IPSA

% Optimization Problem

    nAssets=size(muSymbols,1);
%   Define objective to minimize (Minimize risk given a value of Risk)
    objFn=w'*varMat(2:end,2:end)*w;

%   Return wanted
    r=0.0000;
    
%   Define constraints
    Aeq=ones(1,nAssets);beq=1; % Sum of weigths = 1
    Aineq= -muSymbols';bineq=-r; % Desired return
    lb=zeros(nAssets,1);ub=ones(nAssets,1); % Valid range of weigths
    c= zeros(nAssets,1); % Set to zero nonlinear term of objective expected by the algorithm
    
    
%   Define options for optimization method
    options=optimset('Algorithm','interior-point-convex');
    options = optimset(options,'Display','iter','TolFun',1e-10);% type of algorithm, display iteration, define termination tolerance
    tic
    [x1,fval1] = quadprog(varMat(2:end,2:end),c,Aineq,bineq,Aeq,beq,lb,ub,[],options);
    toc
    % Plot results.
    figure
    title(sprintf('Portfolio Weigths for IPSA Components'),'FontSize',14,'FontWeight','bold')
    subplot(2,1,1)
    bar(1:40,x1,'b')
    xlabel('Symbol ID','FontSize',12)
    ylabel('Weigth (per unit)','FontSize',12)
    grid on;

    % Definition of biased criteria to choose symbols
    Groups = blkdiag(ones(1,floor(nAssets/3)),ones(1,floor(nAssets/3)),ones(1,floor(nAssets/3)+1)); % Three groups of continuous symbols. However ramdom selection can be made
    Aineq = [Aineq; -Groups];         % convert to <= constraint
    bineq = [bineq; -0.3*ones(3,1)];  % by changing signs
    
    % Call solver and measure wall-clock time.
    tic
    [x2,fval2] = quadprog(varMat(2:end,2:end),c,Aineq,bineq,Aeq,beq,lb,ub,[],options);
    toc

    % Plot results, superimposed to results from previous problem.
    subplot(2,1,2)
    bar(1:40,x2,'r')
    xlabel('Symbol ID','FontSize',12)
    ylabel('Weigth (per unit)','FontSize',12)
    grid on;
    
    % Overall Title of plot
    ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
    text(0.5, 1,'\bf Distribution of Weigths for Portfolio Composition','HorizontalAlignment','center','VerticalAlignment', 'top','FontSize',14,'FontWeight','bold')


    % Visualize results against mean return of each Symbols
    [x1 x2 muSymbols]
    
% 
%         % code to check synchrony of data
%         a=[cube(:,1,1) cube(:,5,1) cube(:,5,2)]