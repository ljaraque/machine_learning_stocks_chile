function  [X_norm,mu,sigma]  = featureNormalize( X )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

X_norm = X;
mu = zeros(1, size(X, 2));
sigma = zeros(1, size(X, 2));



mu=mean(X); %mu=mean(X)
sigma=std(X); % If I use this sigma=std(X,1) specifying axis the std gives failed value CHECK WHY

% REASON:     Y = std(X,1) normalizes by N and produces the square root of the second. moment of the sample about its mean.  std(X,0) is the same as std(X).

[height width]=size(X);

mumx=repmat(mu,height,1);
X_norm=X_norm-mumx;

sigmamx=repmat(sigma,height,1);
X_norm=X_norm./sigmamx;

end

