%% Script_Chile_Market


%% Input parameters

% Time period

initialDate=datenum(2012,1,1);
finishDate=datenum(2012,12,31);

% Symbols to analyze
% Output is a list of strings

symbolsToAnalyze={'all'};

% Fetch data from cloud (Bolsa de Santiago and Yahoo)
% Output is a 3D matrix with IPSA and its components prices (Date,Open,High,Low,Close,Volume)

getPrices(symbolsToAnalyze);

% 
