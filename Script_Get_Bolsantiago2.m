clear all; clc, close all;
%slCharacterEncoding('UTF-8')

% Get data from bolsadesantiago
ipsaList=importdata('./data/IdxIPSA.csv');
symbols_to_analyze={'CHILE','BCI'}; % Use 'all' to update all symbols

% Definition of time period
initDate=datenum(2012,12,1)
initDateStr=datestr(initDate,'yyyymmdd')
[Yi,Mi,Di]=datevec(initDateStr,'yyyymmdd')


finishDate=datenum(2012,6,30)
finishDateStr=datestr(finishDate,'yyyymmdd')
[Yf,Mf,Df]=datevec(finishDateStr,'yyyymmdd')



if strcmp(symbols_to_analyze,'all')==1
    symbolList=ipsaList';
else%% else if symbols_to_analyze~='all'
        for i=1:size(symbols_to_analyze,1)
            [i,j]=find(strcmp(ipsaList,symbols_to_analyze(i)));
            if isempty([i j])==1
                fprintf(1,'IPSA Index does not contain symbol: %s', char(symbols_to_analyze(i))); fprintf('\n');
                fprintf(1,'Please check and retry'); fprintf('\n');
                return
            end
        end
        symbolList=symbols_to_analyze;
end


for i=1:size(symbolList,2)
    symbol=symbolList(i);

    url=strcat('http://www.bolsadesantiago.com/Theme/Data/Historico.aspx?Symbol=',symbol,'&dividendo=S');
    filename=strcat('./data/',symbol,'2.csv');
    urlwrite(char(url),char(filename));
end


% Modify files



for i=1:size(symbolList,2)
    symbol=symbolList(i);
    
    filename=strcat('./data/',symbol,'2.csv');
    filename2=strcat('./data/',symbol,'.csv');
    
    fid1 = fopen(char(filename), 'r','n');
    fid = fopen(char(filename2), 'w','n');

    tline = fgetl(fid1);

    while ischar(tline)
        tline=unicode2native(tline);
        tline(tline==0)=[];
        %tline=native2unicode(tline);
        tline=char(tline);
        tline = strrep(tline, ',', '.');
        tline = strrep(tline, ';', ',');
        fprintf(fid, '%s\n', tline);
        tline = fgetl(fid1);    
    end
    
    fclose(fid1);
    fclose(fid);
    delete(char(filename));
    
end

% Open file of the first symbol to generate timeVector
symbol=symbolList(1);
filename=strcat('./data/',symbol,'.csv');
stock=importdata(char(filename));

% Find next present initDate if the specified one is not in the list
i=1;
while ismember(stock.data(:,1),str2num(initDateStr))==0
    initDateStr=datestr(initDate+i,'yyyymmdd');
    i=i+1;
end
idxInitDate=ind2sub(size(stock.data(:,1)),find(ismember(stock.data(:,1),str2num(initDateStr))))

% Find previous finishDate if the specified one is not in the list
i=1;
while ismember(stock.data(:,1),str2num(finishDateStr))==0
    finishDateStr=datestr(finishDate-i,'yyyymmdd');
    i=i+1;
end
idxFinishDate=ind2sub(size(stock.data(:,1)),find(ismember(stock.data(:,1),str2num(finishDateStr))))

% Generation of timestamps
[tvecY,tvecM,tvecD]=datevec(num2str(stock.data(idxInitDate:idxFinishDate,1)),'yyyymmdd');
vecNumDate=datenum(tvecY,tvecM,tvecD);
timeVector=datestr(vecNumDate,'dd-mmm-yyyy');

% Initialize timeseries collection 

for i=1:size(symbolList,2)
    
    symbol=symbolList(i);

    filename=strcat('./data/',symbol,'.csv');
    stock=importdata(char(filename));
    cube(:,:,i)=stock.data(idxInitDate:idxFinishDate,:); % Apply flipup if need reverse order
    
                  % Case of using timeseries
%                 % Generation of timeseries collection and add append timeserie of current symbol
%                 %ts=timeseries(stock.data(idxInitDate:idxFinishDate,2:6),timeVector);
%                 %tsc = addts(tsc,ts,symbol);
%                 tsc = tscollection(timeVector,'name',symbol); % Try with vecNumDate to see ticks in plot
%                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,2),timeVector),'open');
%                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,3),timeVector),'high');
%                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,4),timeVector),'low');
%                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,5),timeVector),'close');
%                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,6),timeVector),'volume');
%                 dataBase.(char(symbol))=tsc;

end


timeVector

return
subplot(2,1,1)
tsToPlot=get(dataBase.CHILE,'close');
plot(tsToPlot)
grid on
subplot(2,1,2)
tsToPlot2=get(dataBase.BCI,'close');
plot(tsToPlot2)
grid on


% NOTA: Time en timeseries no contiene fechas OJO revisar.
% NOTA: Revisar c?mo rotar ticks.
% NOTA: implementar b?squeda de fecha inicial y final cuando la especificada no est? en la lista de fechas.


tick_index = tsToPlot2.Time(1):20:tsToPlot2.Time(end); % ticks spacing
tick_label = datestr(tsToPlot2.Time(1:20:length(tsToPlot2.Time)), 'yyyy.mm.dd');
set(gca,'XTick',tick_index);
set(gca, 'XTickLabel', tick_label);


% Access Timeseries Collection



% Create timestamps
% datestr(datenum(num2str(cube(:,1,1)),'yyyymmdd'),'yyyy-mm-dd')

% 
% plot(cube(:,5,1)/cube(1,5,1),'r')
% hold on
% plot(cube(:,5,2)/cube(1,5,2),'r')
