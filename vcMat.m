function [ varCovarMat, corrMat ] = vcMat( X )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

stdMat=(std(X,0,1))'*std(X,0,1);

varCovarMat=cov(X);
corrMat=varCovarMat./stdMat;

end

