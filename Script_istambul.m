%% Statistical study of data
istambul=importdata('data_akbilgic.csv');
studyData=istambul.data;

[varMat,corrMat]=vcMat(studyData); % Just for reference and debugging
varMat
corrMat
imshow(mat2gray(corrMat),'InitialMagnification',2000)
