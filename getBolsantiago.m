function [] = getBolsantiago( symbols_to_analyze)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
% Get data from bolsadesantiago
ipsaList=importdata('./data/IdxIPSA.csv');
%symbols_to_analyze={'BCI','ENTEL'};

if strcmp(symbols_to_analyze,'all')==1
    symbolList=ipsaList;
else%% else if symbols_to_analyze~='all'
        for i=1:size(symbols_to_analyze,1)
            [i,j]=find(strcmp(ipsaList,symbols_to_analyze(i)));
            if isempty([i j])==1
                fprintf(1,'IPSA Index does not contain symbol: %s', char(symbols_to_analyze(i))); fprintf('\n');
                fprintf(1,'Please check and retry'); fprintf('\n');
                return
            end
        end
        symbolList=symbols_to_analyze;
end


for i=1:size(symbolList,2)
    symbol=symbolList(i);

    url=strcat('http://www.bolsadesantiago.com/Theme/Data/Historico.aspx?Symbol=',symbol,'&dividendo=S');
    filename=strcat('./data/',symbol,'2.csv');
    urlwrite(char(url),char(filename));
end


% Modify files



for i=1:size(symbolList,2)
    symbol=symbolList(i);
    
    filename=strcat('./data/',symbol,'2.csv');
    filename2=strcat('./data/',symbol,'.csv');
    
    fid1 = fopen(char(filename), 'r','n');
    fid = fopen(char(filename2), 'w','n');

    tline = fgetl(fid1);

    while ischar(tline)
        tline=unicode2native(tline);
        tline(tline==0)=[];
        %tline=native2unicode(tline);
        tline=char(tline);
        tline = strrep(tline, ',', '.');
        tline = strrep(tline, ';', ',');
        fprintf(fid, '%s\n', tline);
        tline = fgetl(fid1);    
    end
    
    fclose(fid1);
    fclose(fid);
    delete(char(filename));
    
    
        %     
        %     %Data = fileread(char(filename));
        %     Data=char(importdata(char(filename)));
        %     Data = regexprep(Data,',','.');
        % %     Data = strrep(Data, ',', '.');
        %     Data = regexprep(Data,';',',');
        % %     Data = strrep(Data, ';', ',');
        % %     Data = regexprep(Data,symbol,'');
        % %     Data = strrep(Data,' F e c h a , O p e n , H i g h , L o w , C l o s e , V o l u m e ','');
        %     FID = fopen(char(filename), 'w');
        %     fwrite(FID, char(Data), 'char');
        %     fclose(FID);
end


end

