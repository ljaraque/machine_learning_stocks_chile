clear all; clc, close all;
%slCharacterEncoding('UTF-8')
%% INPUT DATA
    % Update Database?
    updateData=0;       % 0: Do not update      ; 1: Update
    
    % Data Sanity types 
        % Fill back type
        fillBack=1;         % 0: Fill with zeros    ; 1: Fill back with the first value

        % Fill forward type
        fillForward=1;      % 0: Fill with zeros    ; 1: Fill forward with the last value
    
        % Fill gap types
        fillGap=1;          % 0: Fill with zeros    ; 1: Fill with last     ; 2: Fill with next
    % Definition of time period
    initDate=datenum(2012,1,1);
    initDateStr=datestr(initDate,'yyyymmdd');
    [Yi,Mi,Di]=datevec(initDateStr,'yyyymmdd');
    finishDate=datenum(2012,12,31);
    finishDateStr=datestr(finishDate,'yyyymmdd');
    [Yf,Mf,Df]=datevec(finishDateStr,'yyyymmdd');
    % Get data from bolsadesantiago
    ipsaList=importdata('./data/IdxIPSA.csv');
    
    symbols_to_analyze={'all'}; % Use 'all' to update all symbols
    % NOTE: first symbol defines the timestamps. Make sure it includes all trading days of the period
    
%% GENERATION OF SYMBOL LIST
if strcmp(symbols_to_analyze,'all')==1
    symbolList=ipsaList';
else%% else if symbols_to_analyze~='all'
        for i=1:size(symbols_to_analyze,1)
            [i,j]=find(strcmp(ipsaList,symbols_to_analyze(i)));
            if isempty([i j])==1
                fprintf(1,'IPSA Index does not contain symbol: %s', char(symbols_to_analyze(i))); fprintf('\n');
                fprintf(1,'Please check and retry'); fprintf('\n');
                return
            end
        end
        symbolList=symbols_to_analyze;
end

fprintf(1,'Analyzing from %s to %s', initDateStr, finishDateStr); fprintf('\n\n\n');

%% Fetch and Update data from Bolsa de Santiago if chosen

    if updateData==1
                fprintf(1,'Extracting Data from Bolsa de Santiago ...'); fprintf('\n\n\n');
                for i=1:size(symbolList,2)
                    symbol=symbolList(i);
                    url=strcat('http://www.bolsadesantiago.com/Theme/Data/Historico.aspx?Symbol=',symbol,'&dividendo=S');
                    filename=strcat('./data/',symbol,'2.csv');
                    urlwrite(char(url),char(filename));
                end
                % Modify files
                fprintf(1,'Converting files to standard format ...'); fprintf('\n\n\n');

                for i=1:size(symbolList,2)
                    symbol=symbolList(i);
                    filename=strcat('./data/',symbol,'2.csv');
                    filename2=strcat('./data/',symbol,'.csv');
                    fid1 = fopen(char(filename), 'r','n');
                    fid = fopen(char(filename2), 'w','n');
                    tline = fgetl(fid1);
                    while ischar(tline)
                        tline=unicode2native(tline);
                        tline(tline==0)=[];
                        %tline=native2unicode(tline);
                        tline=char(tline);
                        tline = strrep(tline, ',', '.');
                        tline = strrep(tline, ';', ',');
                        fprintf(fid, '%s\n', tline);
                        tline = fgetl(fid1);    
                    end
                    fclose(fid1);
                    fclose(fid);
                    delete(char(filename));
                end
    end


%% TIMESTAMPS GENERATION

        % Open file of the first symbol to generate timeVector
        symbol=symbolList(1);
        filename=strcat('./data/',symbol,'.csv');
        stock=importdata(char(filename));

        % Find next present initDate if the specified one is not in the list
        i=1;
        while ismember(stock.data(:,1),str2num(initDateStr))==0
            initDateStr=datestr(initDate+i,'yyyymmdd');
            i=i+1;
        end
        idxInitDate=ind2sub(size(stock.data(:,1)),find(ismember(stock.data(:,1),str2num(initDateStr))));

        % Find previous finishDate if the specified one is not in the list
        i=1;
        while ismember(stock.data(:,1),str2num(finishDateStr))==0
            finishDateStr=datestr(finishDate-i,'yyyymmdd');
            i=i+1;
        end
        idxFinishDate=ind2sub(size(stock.data(:,1)),find(ismember(stock.data(:,1),str2num(finishDateStr))));

        % Generation of timestamps
        [tvecY,tvecM,tvecD]=datevec(num2str(stock.data(idxInitDate:idxFinishDate,1)),'yyyymmdd');
        vecNumDate=datenum(tvecY,tvecM,tvecD);
        timeVector=datestr(vecNumDate,'dd-mmm-yyyy');
        
        % Fill first layer 1 of 3D database
        % NOTE: This part assumes that first stock is the one to define the valid timestamps
        cube(:,:,1)=stock.data(idxInitDate:idxFinishDate,:);
        lengthCube=size(cube,1)
        
        
%% FILLING 3D DATABASE
        fprintf(1,'Composing 3D-Stocks Database ...'); fprintf('\n\n\n');

        for i=2:size(symbolList,2) % Starts from 2nd symbol because the first one was alreay filled before this loop
            i
            symbol=symbolList(i);
            filename=strcat('./data/',symbol,'.csv');
            stock=importdata(char(filename));
%             % Position of stock in the time window
            [yearSymbol,monthSymbol,daySymbol]=datevec(num2str(stock.data(:,1)),'yyyymmdd');
            vecNumDateSymbol=datenum(yearSymbol,monthSymbol,daySymbol);
%             
%             % encontrar posicion de fecha inicial de vecNumDate en symbol y llamarle idxInitSymbolSymbol
%             idxInitSymbolSymbol=find(vecNumDateSymbol==vecNumDate(1)); % If there are 2 equal dates there will be an error
%             % encontrar posicion de fecha final de vecNumDate en symbol y llamarle idxFinishSymbolSymbol
%             idxFinishSymbolSymbol=find(vecNumDateSymbol==vecNumDate(end)); % If there are 2 equal dates there will be an error
%             % encontrar posicion de fecha inicial de symbol en vecNumDate y llamarle idxInitSymbolCube
%             if vecNumDateSymbol(1)<vecNumDate(1)
%                 idxInitSymbolCube=vecNumDate(1);
%             else
%                 idxInitSymbolCube=find(vecNumDate==vecNumDateSymbol(1));
%             end
%             % encontrar posicion de fecha final de symbol en vecNumDate y llamarle idxFinishSymbolCube
%             if vecNumDateSymbol(end)>vecNumDate(end)
%                 idxFinishSymbolCube=vecNumDate(end);
%             else
%                 idxFinishSymbolCube=find(vecNumDate==vecNumDateSymbol(end));
%             end
%            
%             difIdxCube=idxFinishSymbolCube-idxInitSymbolCube+1
%             difIdxSymbol=idxFinishSymbolSymbol-idxInitSymbolSymbol+1
            
            for j=1:size(cube,1)
                if isempty(find(vecNumDateSymbol(:,1)==vecNumDate(j)))~=1
                    
                    cube(j,:,i)=stock.data(find(vecNumDateSymbol(:,1)==vecNumDate(j)),:);
                else
        
                end
            end
            
%             
%             if (vecNumDateSymbol(1)<=vecNumDate(1) & vecNumDateSymbol(end)>=vecNumDate(end))==1
%                     1
%                     cube(:,:,i)=stock.data(idxInitSymbolSymbol:idxFinishSymbolSymbol,:); % Apply flipup if need reverse order
%                     
%             elseif (vecNumDateSymbol(1)>vecNumDate(1) & vecNumDateSymbol(end)>=vecNumDate(end))==1 % comienza despues que timestamps
%                     2
%                     cube(idxInitSymbolCube:end,:,i)=stock.data(1:idxFinishSymbolSymbol,:); % Apply flipup if need reverse order
%                     % Add data sanity here (Fill back)
%                     
%             elseif (vecNumDateSymbol(1)<=vecNumDate(1) & vecNumDateSymbol(end)<vecNumDate(end))==1 % termina antes que timestamps
%                     3
%                     cube(1:idxFinishSymbolCube,:,i)=stock.data(idxInitSymbolSymbol:end,:); % Apply flipup if need reverse order
%                     % Add data sanity here (Fill forward)
%                     
%             elseif (vecNumDateSymbol(1)>vecNumDate(1) & vecNumDateSymbol(end)<vecNumDate(end))==1 % comienza despues que timestamps y termina antes que timestamps
%                     4
%                     cube(idxInitSymbolCube:idxFinishSymbolCube,:,i)=stock.data(idxInitSymbolSymbol:idxFinishSymbolSymbol,:);
%                     
%             else
%                     error('Unknown data anomaly');
%             end
              
pause
       % Mode advanced data sanity is pending (fill gaps, detect big daily variations, etc.)
            


                          % Case of using timeseries
        %                 % Generation of timeseries collection and add append timeserie of current symbol
        %                 %ts=timeseries(stock.data(idxInitDate:idxFinishDate,2:6),timeVector);
        %                 %tsc = addts(tsc,ts,symbol);
        %                 tsc = tscollection(timeVector,'name',symbol); % Try with vecNumDate to see ticks in plot
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,2),timeVector),'open');
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,3),timeVector),'high');
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,4),timeVector),'low');
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,5),timeVector),'close');
        %                 tsc=addts(tsc,timeseries(stock.data(idxInitDate:idxFinishDate,6),timeVector),'volume');
        %                 dataBase.(char(symbol))=tsc;
        
        end


timeVector










return
subplot(2,1,1)
tsToPlot=get(dataBase.CHILE,'close');
plot(tsToPlot)
grid on
subplot(2,1,2)
tsToPlot2=get(dataBase.BCI,'close');
plot(tsToPlot2)
grid on


% NOTA: Time en timeseries no contiene fechas OJO revisar.
% NOTA: Revisar c?mo rotar ticks.
% NOTA: implementar b?squeda de fecha inicial y final cuando la especificada no est? en la lista de fechas.


tick_index = tsToPlot2.Time(1):20:tsToPlot2.Time(end); % ticks spacing
tick_label = datestr(tsToPlot2.Time(1:20:length(tsToPlot2.Time)), 'yyyy.mm.dd');
set(gca,'XTick',tick_index);
set(gca, 'XTickLabel', tick_label);


% Access Timeseries Collection



% Create timestamps
% datestr(datenum(num2str(cube(:,1,1)),'yyyymmdd'),'yyyy-mm-dd')

% 
% plot(cube(:,5,1)/cube(1,5,1),'r')
% hold on
% plot(cube(:,5,2)/cube(1,5,2),'r')
